package com.slowik.dhl.users

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.widget.Button
import android.widget.LinearLayout
import com.slowik.dhl.R

/**
 * Created by Tomek on 2015-10-22.
 */
class AddGateDialog(val onClick: (gate: String) -> Unit) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog? {

        val builder = AlertDialog.Builder(getActivity());
        val inflater = getActivity().getLayoutInflater();

        val view = inflater.inflate(R.layout.dialog_gates_add, null)

        val container = view.findViewById(R.id.container) as LinearLayout
        val gates = arrayListOf("Gate 28", "Gate 29", "Gate 30")
        for (gate in gates) {
            val button: Button = inflater.inflate(R.layout.button_gate, null) as Button
            button.setText(gate)
            button.setOnClickListener {
                onClick(gate)
                dismiss()
            }
            container.addView(button)
        }

        builder.setView(view)
                .setTitle("Choose gate:")
        return builder.create();
    }

}