package com.slowik.dhl.users

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.cocosw.bottomsheet.BottomSheet
import com.slowik.dhl.R
import com.slowik.dhl.Utils.FirebaseOperator


class DriverAdapter(val activity: FragmentActivity, private val mDataset: MutableList<DriverAdapter.DriverData>) : RecyclerView.Adapter<DriverAdapter.ViewHolder>() {

    data class DriverData(val name: String, val phone: String, var uid: String?, var default: Boolean) {
        constructor() : this("", "", "", false)
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        lateinit var usernameTV: TextView
        lateinit var phoneTV: TextView
        lateinit var moreIV: ImageView
        lateinit var leftIV: ImageView

        init {
            usernameTV = v.findViewById(R.id.usernameTV) as TextView
            phoneTV = v.findViewById(R.id.passTV) as TextView
            moreIV = v.findViewById(R.id.moreButton) as ImageView
            leftIV = v.findViewById(R.id.leftImage) as ImageView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        val vh = ViewHolder(v)
        return vh
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val name = mDataset[position].name
        val phone = mDataset[position].phone
        val uid = mDataset[position].uid
        val default = mDataset[position].default
        holder.usernameTV.setText(name)
        holder.phoneTV.setText("tel: " + phone)
        if (default) {
            holder.leftIV.setBackgroundResource(R.drawable.ic_drivers_green_52dp)
        } else {
            holder.leftIV.setBackgroundResource(R.drawable.ic_drivers_52dp)
        }
        holder.moreIV.setOnClickListener {
            BottomSheet.Builder(activity).title(name).sheet(R.menu.drivers).listener { dialog, which ->
                if (which == R.id.delete) {
                    FirebaseOperator.removeValue("drivers/" + uid)
                } else if (which == R.id.edit) {
                    showEditDriverDialog(name, phone, uid!!)
                } else if (which == R.id.defaultSet) {

                    FirebaseOperator.oneShotRequest("defaultDriver").subscribe { snapshot ->
                        val oldDriver = snapshot.value
                        FirebaseOperator.setValue("defaultDriver", uid!!)
                        FirebaseOperator.setValue("drivers/" + uid + "/default", true)
                        if (oldDriver != null) {
                            FirebaseOperator.setValue("drivers/" + oldDriver + "/default", false)
                        }
                    }
                }
            }.show()
        }
    }

    override fun getItemCount(): Int {
        return mDataset.size
    }

    private fun showEditDriverDialog(name: String, phone: String, uid: String) {
        val dialog = DriverDialog("Edit", { newname, newphone ->
            if (!newname.isEmpty() && !newphone.isEmpty()) {
                val values = hashMapOf("name" to newname, "phone" to newphone)
                FirebaseOperator.updateValue("drivers/" + uid, values)
            }
        }, name, phone)
        dialog.show(activity.supportFragmentManager, "tag")

    }
}
