package com.slowik.dhl.tasks

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.slowik.dhl.tasks.model.Task
import rx.subjects.PublishSubject


class TaskAdapter(val activity: FragmentActivity, private val mDataset: MutableList<Task>, val clickObservable: PublishSubject<TaskAdapter.TaskListClickInfo>) : RecyclerView.Adapter<TaskAdapter.ViewHolder>() {

    class TaskListClickInfo(val task: Task, val view: View)

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {}

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val v = TaskView(parent.context)
        val vh = ViewHolder(v)
        return vh
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val task = mDataset[position]
        holder.itemView.setOnClickListener { v ->
            val taskListClickInfo = TaskListClickInfo(task, v)
            clickObservable.onNext(taskListClickInfo)
        }
        (holder.itemView as TaskView).bindData(task, activity)

    }

    override fun getItemCount(): Int {
        return mDataset.size
    }


}
