package com.slowik.dhl.tasks

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import com.slowik.dhl.R
import com.slowik.dhl.Utils.FirebaseOperator
import com.slowik.dhl.Utils.hideKeyboard
import com.slowik.dhl.tasks.model.Task
import com.slowik.dhl.users.DriverAdapter
import fr.ganfra.materialspinner.MaterialSpinner
import kotlinx.android.synthetic.activity_task_add.containerET
import kotlinx.android.synthetic.activity_task_add.scrollView
import java.util.*

class AddTaskActivity : AppCompatActivity() {


    var driverSpinner1: MaterialSpinner? = null
    var driverSpinner2: MaterialSpinner? = null

    private val driversMap: HashMap<String, DriverAdapter.DriverData> = hashMapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_add)
        setSupportActionBar(findViewById(R.id.toolbar) as Toolbar?)
        supportActionBar.setDisplayHomeAsUpEnabled(true)

        scrollView.setOnTouchListener { view, motionEvent ->
            hideKeyboard()
            false
        }

        driverSpinner1 = findViewById(R.id.driverSpinner1) as MaterialSpinner?
        driverSpinner2 = findViewById(R.id.driverSpinner2) as MaterialSpinner?


        driverSpinner1?.setOnTouchListener { view, motionEvent ->
            hideKeyboard()
            false
        }


        val drivers = arrayListOf<String>()
        var defaultDriver: String? = null
        FirebaseOperator.oneShotRequest("drivers").subscribe { snapshot ->
            for (subsnapshot in snapshot.children) {
                val data = subsnapshot?.getValue(DriverAdapter.DriverData::class.java)
                if (data != null) {
                    data.uid = subsnapshot.key
                    val displayData = data.name + " (" + data.phone + ")"
                    driversMap.put(displayData, data)
                    drivers.add(displayData)
                    if (data.default) {
                        defaultDriver = displayData
                    }
                }
            }
            populateSpinner(drivers, driverSpinner1)
            populateSpinner(drivers, driverSpinner2)
            if (defaultDriver != null) {
                val index = drivers.indexOfRaw(defaultDriver) + 1
                driverSpinner1?.setSelection(index)
                driverSpinner2?.setSelection(index)
            }
        }

    }

    private fun populateSpinner(items: List<String>, spinner: MaterialSpinner?) {
        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner?.setAdapter(adapter)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        getMenuInflater().inflate(R.menu.task_add, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        if (id == R.id.done) {
            if (validateInput()) {
                val driver1 = driverSpinner1?.selectedItem as String
                val driver2 = driverSpinner2?.selectedItem as String
                val task = Task(containerET.text.toString(),
                        driversMap.get(driver1)?.uid ?: "",
                        driversMap.get(driver2)?.uid ?: "")

                FirebaseOperator.pushValue("tasks", task).subscribe {
                    onBackPressed()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun validateInput(): Boolean {
        return !containerET.text.toString().isEmpty() &&
                driversMap.get(driverSpinner1?.selectedItem as String) != null &&
                driversMap.get(driverSpinner2?.selectedItem as String) != null
    }

}