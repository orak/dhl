package com.slowik.dhl.users

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.widget.EditText
import com.slowik.dhl.R

/**
 * Created by Tomek on 2015-10-22.
 */
class DriverDialog(val buttonText: String, val onClick: (name: String, phone: String) -> Unit, val oldName: String, val oldPhone: String) : DialogFragment() {

    constructor(buttonText: String, onClick: (username: String, phone: String) -> Unit) : this(buttonText, onClick, "", "")

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog? {

        val builder = AlertDialog.Builder(getActivity());
        val inflater = getActivity().getLayoutInflater();

        val view = inflater.inflate(R.layout.dialog_driver, null)

        val nameET = view.findViewById(R.id.usernameET) as EditText
        val phoneET = view.findViewById(R.id.phoneET) as EditText

        nameET.setText(oldName)
        phoneET.setText(oldPhone)

        builder.setView(view)
                .setPositiveButton(buttonText, { dialog, id -> onClick(nameET.text.toString(), phoneET.text.toString()) })
        return builder.create();
    }

}