package com.slowik.dhl.tasks.model;

import com.slowik.dhl.App;
import com.slowik.dhl.Utils.Prefs;

import java.util.Arrays;
import java.util.List;

public class Task {

    public String containerNumber = "";
    public long createTimestamp = System.currentTimeMillis();
    public String gate = "";
    public String actualStatus = TaskUtils.statuses.get(0);
    public List<TaskAction> actions = Arrays.asList(new TaskAction(TaskUtils.ACTION_CREATE_TASK, Prefs.Companion.load(App.context, "username"), System.currentTimeMillis()));
    public int linesOccupied = 0;
    public int linesSent = 0;
    public String firstDriver = "";
    public String secondDriver = "";
    public String uid = null;

    public Task(String containerNumber, long createTimestamp, String gate, String actualStatus, List<TaskAction> actions, int linesOccupied, int linesSent, String firstDriver, String secondDriver, String uid) {
        this.containerNumber = containerNumber;
        this.createTimestamp = createTimestamp;
        this.gate = gate;
        this.actualStatus = actualStatus;
        this.actions = actions;
        this.linesOccupied = linesOccupied;
        this.linesSent = linesSent;
        this.firstDriver = firstDriver;
        this.secondDriver = secondDriver;
        this.uid = uid;
    }

    public Task() {
    }

    public Task(String containerNumber, String firstDriver, String secondDriver) {
        this.containerNumber = containerNumber;
        this.firstDriver = firstDriver;
        this.secondDriver = secondDriver;
    }

    public Task(Task otherTask) {
        this.containerNumber = otherTask.containerNumber;
        this.createTimestamp = otherTask.createTimestamp;
        this.gate = otherTask.gate;
        this.actualStatus = otherTask.actualStatus;
        this.actions = otherTask.actions;
        this.linesOccupied = otherTask.linesOccupied;
        this.linesSent = otherTask.linesSent;
        this.firstDriver = otherTask.firstDriver;
        this.secondDriver = otherTask.secondDriver;
        this.uid = otherTask.uid;
    }
}


