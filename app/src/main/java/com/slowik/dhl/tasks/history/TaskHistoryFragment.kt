package com.slowik.dhl.tasks.history

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.slowik.dhl.R
import com.slowik.dhl.tasks.model.Task
import com.slowik.dhl.tasks.model.TaskAction
import java.util.*

class TaskHistoryFragment(val task: Task) : Fragment() {

    private var adapter: TaskHistoryAdapter? = null

    private val items: MutableList<TaskAction> = arrayListOf()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_fab_list, container, false)

        val fab = view?.findViewById(R.id.fab)
        val recyclerView = view?.findViewById(R.id.recyclerView) as RecyclerView

        fab?.visibility = View.GONE

        recyclerView.setHasFixedSize(true)
        val llm = LinearLayoutManager(context)
        recyclerView.setLayoutManager(llm)
        items.addAll(task.actions)
        adapter = TaskHistoryAdapter(activity, items as ArrayList<TaskAction>)
        recyclerView.setAdapter(adapter)

        return view
    }

    fun updateTask(task: Task) {
        adapter?.updateItems(task.actions)
    }

}