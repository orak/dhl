package com.slowik.dhl.Utils

import android.content.Context

public class Prefs {

    companion object {

        fun save(context: Context, key: String, data: String?) {
            val sharedPref = context.getSharedPreferences("com.slowik.dhl", Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putString(key, data)
            editor.apply()
        }

        fun load(context: Context, key: String): String? {
            val sharedPref = context.getSharedPreferences("com.slowik.dhl", Context.MODE_PRIVATE)
            return sharedPref.getString(key, null)
        }

    }

}