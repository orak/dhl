package com.slowik.dhl.tasks.edit

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.firebase.client.ChildEventListener
import com.firebase.client.DataSnapshot
import com.firebase.client.FirebaseError
import com.google.gson.Gson
import com.kogitune.activity_transition.ActivityTransition
import com.kogitune.activity_transition.ExitActivityTransition
import com.slowik.dhl.R
import com.slowik.dhl.Utils.FirebaseOperator
import com.slowik.dhl.tasks.TaskView
import com.slowik.dhl.tasks.history.TaskHistoryFragment
import com.slowik.dhl.tasks.model.Task
import com.slowik.dhl.users.AddGateDialog


class EditTaskActivity : AppCompatActivity() {

    private var exitTransition: ExitActivityTransition? = null
    var task: Task? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_edit)
        setSupportActionBar(findViewById(R.id.toolbar) as Toolbar?)
        supportActionBar.setDisplayHomeAsUpEnabled(true)

        val taskView: TaskView = findViewById(R.id.taskView) as TaskView

        task = Gson().fromJson(getIntent().getStringExtra("task"), Task::class.java)

        taskView.bindData(task, this)

        taskView.containerNumberTV.setOnClickListener {
            ContainerDialog({ containerNumber ->
                FirebaseOperator.setValue("tasks/" + task?.uid + "/containerNumber", containerNumber)
            }, task?.containerNumber).show(supportFragmentManager, "tag")
        }

        taskView.gateTV.setOnClickListener {
            val dialog = AddGateDialog({ gate ->
                FirebaseOperator.setValue("tasks/" + task?.uid + "/gate", gate)
            })
            dialog.show(supportFragmentManager, "tag2")
        }

        exitTransition = ActivityTransition.with(getIntent()).to(taskView).duration(300).start(savedInstanceState)

        val transaction = supportFragmentManager.beginTransaction()
        val taskHistoryFragment = TaskHistoryFragment(task as Task)
        transaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
        transaction.replace(R.id.historyContainer, taskHistoryFragment).commit()

        val ref = FirebaseOperator.firebaseRef.child("tasks").child(task?.uid)
        val listener = object : ChildEventListener {
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onCancelled(p0: FirebaseError?) {
            }

            override fun onChildAdded(snapshot: DataSnapshot?, p1: String?) {
            }


            override fun onChildChanged(snapshot: DataSnapshot?, p1: String?) {
                FirebaseOperator.oneShotRequest("tasks/" + task?.uid).subscribe { snapshot ->
                    val data = snapshot?.getValue(Task::class.java)
                    if (data != null) {
                        data.uid = snapshot?.key
                        task = data
                        taskView.bindData(task, this@EditTaskActivity)
                        taskHistoryFragment.updateTask(task as Task)
                    }
                }

            }

            override fun onChildRemoved(snapshot: DataSnapshot?) {
                finish()
            }
        }
        ref?.addChildEventListener(listener)


    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        exitTransition?.exit(this)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out)
        transaction.remove(supportFragmentManager.findFragmentById(R.id.historyContainer)).commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        getMenuInflater().inflate(R.menu.task_edit, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        if (id == R.id.delete) {
            FirebaseOperator.removeValue("tasks/" + task?.uid)
        } else if (id == R.id.drivers) {
            val dialog = DriversDialog({ firstDriver, secondDriver ->
                FirebaseOperator.setValue("tasks/" + task?.uid + "/firstDriver", firstDriver)
                FirebaseOperator.setValue("tasks/" + task?.uid + "/secondDriver", secondDriver)
            }, task?.firstDriver, task?.secondDriver)
            dialog.show(supportFragmentManager, "tag3")
        }
        return super.onOptionsItemSelected(item)
    }

}