package com.slowik.dhl.users

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.widget.EditText
import com.slowik.dhl.R

/**
 * Created by Tomek on 2015-10-22.
 */
class OccupiedLinesDialog(val onClick: (occupiedLines: String) -> Unit) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog? {

        val builder = AlertDialog.Builder(getActivity());
        val inflater = getActivity().getLayoutInflater();

        val view = inflater.inflate(R.layout.dialog_lines_occupied, null)

        val linesET = view.findViewById(R.id.linesET) as EditText

        builder.setView(view)
                .setPositiveButton("OK", { dialog, id -> onClick(linesET.text.toString()) })
        return builder.create();
    }

}