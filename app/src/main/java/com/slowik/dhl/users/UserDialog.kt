package com.slowik.dhl.users

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.widget.EditText
import com.slowik.dhl.R

/**
 * Created by Tomek on 2015-10-22.
 */
class UserDialog(val buttonText: String, val onClick: (username: String, password: String) -> Unit, val oldName: String, val oldPass: String) : DialogFragment() {

    constructor(buttonText: String, onClick: (username: String, password: String) -> Unit) : this(buttonText, onClick, "", "")

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog? {

        val builder = AlertDialog.Builder(getActivity());
        val inflater = getActivity().getLayoutInflater();

        val view = inflater.inflate(R.layout.dialog_user, null)

        val usernameET = view.findViewById(R.id.usernameET) as EditText
        val passwordET = view.findViewById(R.id.passwordET) as EditText

        usernameET.setText(oldName)
        passwordET.setText(oldPass)

        builder.setView(view)
                .setPositiveButton(buttonText, { dialog, id -> onClick(usernameET.text.toString(), passwordET.text.toString()) })
        return builder.create();
    }

}