package com.slowik.dhl

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.TextView
import com.slowik.dhl.Utils.Prefs
import com.slowik.dhl.tasks.TasksFragment

/**
 * Created by Tomek on 2015-10-26.
 */
class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var drawer: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()

        val navigationView: NavigationView = findViewById(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentLayout, TasksFragment()).commit()

        val header = LayoutInflater.from(this).inflate(R.layout.nav_header_admin_main, null);
        navigationView.addHeaderView(header);
        val userNameTV: TextView = header.findViewById(R.id.navUserName) as TextView
        userNameTV.setText("User: " + Prefs.load(this, "username"))
        val pInfo = getPackageManager().getPackageInfo(getPackageName(), 0)
        val version = pInfo.versionName
        val appVersionTV: TextView = header.findViewById(R.id.navAppVersion) as TextView
        appVersionTV.setText("v" + version)
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.nav_logout) {
            logout()
        }
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    private fun logout() {
        Prefs.save(this, "username", null)
        Prefs.save(this, "pass", null)
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}