package com.slowik.dhl

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.slowik.dhl.Utils.FirebaseOperator
import com.slowik.dhl.Utils.Prefs
import com.slowik.dhl.Utils.hideKeyboard
import kotlinx.android.synthetic.activity_login.login_progress
import kotlinx.android.synthetic.activity_login.passwordET
import kotlinx.android.synthetic.activity_login.sign_in_button
import kotlinx.android.synthetic.activity_login.usernameET
import rx.android.schedulers.AndroidSchedulers

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //        FirebaseOperator.createUser(App.adminName, "superadmin")

        sign_in_button.setOnClickListener {
            hideKeyboard()
            login()
        }


        FirebaseOperator.loading().observeOn(AndroidSchedulers.mainThread()).subscribe { loading ->
            login_progress.visibility = if (loading) View.VISIBLE else View.GONE
            sign_in_button.isEnabled = !loading
        }


        // TODO Do IT RIGHT WAY!!!!
        val username = Prefs.load(this, "username")
        val pass = Prefs.load(this, "pass")
        if (username != null && pass != null) {
            usernameET.setText(username)
            passwordET.setText(pass)
            login()
        }

    }

    private fun login() {
        val username = usernameET.text.toString()
        FirebaseOperator.login(username, passwordET.text.toString()).subscribe { result ->
            Prefs.save(this, "username", username)
            Prefs.save(this, "pass", passwordET.text.toString())
            if (App.isAdmin(username)) {
                startActivity(Intent(this, AdminMainActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        }
    }


}

