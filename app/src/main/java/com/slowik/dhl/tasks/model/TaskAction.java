package com.slowik.dhl.tasks.model;

/**
 * Created by Tomek on 2015-11-03.
 */
public class TaskAction {

    public String actionType = "";
    public String submitedBy = "";
    public long submitedTimestamp = 0;

    public TaskAction() {
    }

    public TaskAction(String actionType, String submitedBy, long submitedTimestamp) {
        this.actionType = actionType;
        this.submitedBy = submitedBy;
        this.submitedTimestamp = submitedTimestamp;
    }
}
