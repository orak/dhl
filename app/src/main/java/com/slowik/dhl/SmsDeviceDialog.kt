package com.slowik.dhl

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.widget.TextView
import com.slowik.dhl.Utils.UIDGenerator

/**
 * Created by Tomek on 2015-10-22.
 */
class SmsDeviceDialog(val smsDevice: String,
                      val onYes: () -> Unit,
                      val onNo: () -> Unit) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog? {

        val builder = AlertDialog.Builder(getActivity());
        val inflater = getActivity().getLayoutInflater();

        val view = inflater.inflate(R.layout.dialog_sms_device, null)
        val smsTV: TextView = view.findViewById(R.id.smsTV) as TextView

        if (smsDevice.isEmpty()) {
            smsTV.setText("NONE device is used as sending sms to drivers device. Do you want to use this one?")
        } else {
            val isUsed = smsDevice == UIDGenerator.id(activity)
            if (isUsed) {
                smsTV.setText("THIS device is used as sending sms to drivers device. Do you want to still use it?")
            } else {
                smsTV.setText("ANOTHER device is used as sending sms to drivers device. Do you want to use this one?")
            }
        }


        builder.setView(view)
                .setPositiveButton("YES", { dialog, id -> onYes() })
                .setNegativeButton("NO", { dialog, id -> onNo() })
        return builder.create()
    }

}