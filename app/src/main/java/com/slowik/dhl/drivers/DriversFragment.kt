package com.slowik.dhl.drivers

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.client.ChildEventListener
import com.firebase.client.DataSnapshot
import com.firebase.client.Firebase
import com.firebase.client.FirebaseError
import com.slowik.dhl.R
import com.slowik.dhl.Utils.FirebaseOperator
import com.slowik.dhl.users.DriverAdapter
import com.slowik.dhl.users.DriverDialog
import java.util.*

class DriversFragment : Fragment() {

    private var ref: Firebase? = null
    private var listener: ChildEventListener? = null

    private var adapter: DriverAdapter? = null
    private var items: MutableList<DriverAdapter.DriverData> = arrayListOf()

    val itemsMap: HashMap<String?, DriverAdapter.DriverData> = hashMapOf()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_fab_list, container, false)

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fab = view?.findViewById(R.id.fab)
        val recyclerView = view?.findViewById(R.id.recyclerView) as RecyclerView

        fab?.setOnClickListener {
            showCreateDriverDialog()
        }

        recyclerView.setHasFixedSize(true)
        val llm = LinearLayoutManager(context);
        recyclerView.setLayoutManager(llm);
        adapter = DriverAdapter(activity, items as ArrayList<DriverAdapter.DriverData>)
        recyclerView.setAdapter(adapter)

        ref = FirebaseOperator.firebaseRef.child("drivers")
        listener = object : ChildEventListener {
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onCancelled(p0: FirebaseError?) {
            }

            override fun onChildAdded(snapshot: DataSnapshot?, p1: String?) {
                val data = snapshot?.getValue(DriverAdapter.DriverData::class.java)
                if (data != null) {
                    data.uid = snapshot?.key
                    itemsMap.put(data.uid, data)
                    items?.add(data)
                    adapter?.notifyDataSetChanged()
                }
            }

            override fun onChildChanged(snapshot: DataSnapshot?, p1: String?) {
                val data = snapshot?.getValue(DriverAdapter.DriverData::class.java)
                if (data != null) {
                    data.uid = snapshot?.key
                    var index = items.indexOfRaw(itemsMap[data.uid])
                    if (index > -1) {
                        items.removeAt(index)
                        itemsMap.remove(data.uid)
                        items.add(index, data)
                        itemsMap.put(data.uid, data)
                        adapter?.notifyDataSetChanged()
                    }
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot?) {
                val data = snapshot?.getValue(DriverAdapter.DriverData::class.java)
                if (data != null) {
                    data.uid = snapshot?.key
                    items?.remove(data)
                    adapter?.notifyDataSetChanged()
                }
            }
        }
        ref?.addChildEventListener(listener)
    }

    private fun showCreateDriverDialog() {
        val dialog = DriverDialog("Create", { name, phone ->
            val values = hashMapOf("name" to name, "phone" to phone)
            FirebaseOperator.pushValue("drivers", values)
        })
        dialog.show(activity.supportFragmentManager, "tag")

    }

}