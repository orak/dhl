package com.slowik.dhl

import android.app.Application
import android.app.Application.AUDIO_SERVICE
import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.telephony.SmsManager
import com.firebase.client.Firebase


class App : Application() {

    companion object {
        private val adminName = "Krzysztof"
        @JvmField
        var context: Context? = null

        fun playNotificationSound() {
            var mp = MediaPlayer.create(context, R.raw.notify2)
            val mAudioManager = context?.getSystemService(AUDIO_SERVICE) as AudioManager
            val originalVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC)
            mp.setOnCompletionListener {
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, originalVolume, 0)
                mp.reset();
                mp.release();
                mp = null;
            }
            mp.setVolume(1f, 1f)
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0)
            mp.start()
        }

        fun sendSms(phone: String, message: String) {
            val manager = SmsManager.getDefault();
            manager.sendTextMessage(phone, null, message, null, null)
        }

        fun isAdmin(username: String?): Boolean {
            return username?.toLowerCase() == adminName.toLowerCase()
        }
    }

    override fun onCreate() {
        context = applicationContext
        Firebase.setAndroidContext(this)
        Firebase.getDefaultConfig().setPersistenceEnabled(true);
    }

}