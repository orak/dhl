package com.slowik.dhl.Utils

import android.util.Log
import com.firebase.client.*
import rx.Observable
import rx.subjects.BehaviorSubject
import rx.subjects.PublishSubject

internal object FirebaseOperator {

    val firebaseRef: Firebase = Firebase("https://dhl.firebaseio.com/")
    //            val firebaseRef: Firebase = Firebase("https://dazzling-heat-1058.firebaseio.com/")

    val loadingSubject = BehaviorSubject.create(false)

    val subjectsMap: MutableList<Any> = arrayListOf()

    val errorSubject = PublishSubject.create<FirebaseError>()

    fun onError(): Observable<FirebaseError> {
        return errorSubject
    }

    fun loading(): Observable<Boolean> {
        return loadingSubject
    }

    fun createUser(username: String, password: String): Observable<Map<String, Any>> {
        val subject = createSubject<Map<String, Any>>()
        firebaseRef.createUser(username + "@dhlslowikapp.com", password, object : Firebase.ValueResultHandler<Map<String, Any>> {
            override fun onSuccess(result: Map<String, Any>) {
                val userMap = hashMapOf("username" to username, "password" to password)
                firebaseRef.child("users/" + result.get("uid")).setValue(userMap, BasicOnCompleteListener(result, subject))
            }

            override fun onError(firebaseError: FirebaseError) = basicErrorMethod(firebaseError, subject)
        })
        return subject
    }

    fun removeUser(username: String, password: String, uid: String): Observable<Boolean> {
        val subject = createSubject<Boolean>()
        firebaseRef.removeUser(username + "@dhlslowikapp.com", password, object : Firebase.ResultHandler {
            override fun onSuccess() {
                basicSuccessMethod(true, subject)
                removeValue("users/" + uid)
            }

            override fun onError(firebaseError: FirebaseError) = basicErrorMethod(firebaseError, subject)
        })
        return subject
    }

    fun login(username: String, password: String): Observable<AuthData> {
        val subject = createSubject<AuthData>()
        firebaseRef.authWithPassword(username + "@dhlslowikapp.com", password, object : Firebase.AuthResultHandler {
            override fun onAuthenticated(authData: AuthData) = basicSuccessMethod(authData, subject)
            override fun onAuthenticationError(firebaseError: FirebaseError) = basicErrorMethod(firebaseError, subject)
        })
        return subject
    }

    fun oneShotRequest(childRef: String): Observable<DataSnapshot> {
        val subject = createSubject<DataSnapshot>()
        firebaseRef.child(childRef).keepSynced(true)
        firebaseRef.child(childRef).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) = basicSuccessMethod(snapshot, subject)
            override fun onCancelled(firebaseError: FirebaseError) = basicErrorMethod(firebaseError, subject)
        })
        return subject
    }

    fun removeValue(childRef: String): Observable<Boolean> {
        val subject = createSubject<Boolean>()
        val refToRemove = firebaseRef.child(childRef)
        refToRemove.removeValue(BasicOnCompleteListener(true, subject))
        return subject
    }

    fun setValue(childRef: String, value: Any): Observable<Boolean> {
        val subject = createSubject<Boolean>()
        val refToSet = firebaseRef.child(childRef)
        refToSet.setValue(value, BasicOnCompleteListener(true, subject))
        return subject
    }

    fun updateValue(childRef: String, updatedChildren: Map<String, Any>): Observable<Boolean> {
        val subject = createSubject<Boolean>()
        val refToUpdate = firebaseRef.child(childRef)
        refToUpdate.updateChildren(updatedChildren, BasicOnCompleteListener(true, subject))
        return subject
    }

    fun pushValue(childRef: String, values: Any): Observable<String> {
        val subject = createSubject<String>()
        val refToPush = firebaseRef.child(childRef)
        val newPostRef = refToPush.push()
        newPostRef.setValue(values, object : Firebase.CompletionListener {
            override fun onComplete(firebaseError: FirebaseError?, p1: Firebase?) {
                if (firebaseError != null) {
                    basicErrorMethod(firebaseError, subject)
                } else {
                    basicSuccessMethod(newPostRef.key, subject)
                }
            }
        })
        return subject
    }

    private fun <T> basicErrorMethod(firebaseError: FirebaseError, subject: PublishSubject<T>) {
        Log.d("mytest", "Error: " + firebaseError.message + " " + firebaseError.details)
        errorSubject.onNext(firebaseError)
        completeSubject(subject)
    }

    private fun <T> basicSuccessMethod(result: T, subject: PublishSubject<T>) {
        Log.d("mytest", "Firebase Success")
        subject.onNext(result)
        completeSubject(subject)
    }

    private fun <T> createSubject(): PublishSubject<T> {
        val subject = PublishSubject.create<T>()
        subjectsMap.add(subject)
        loadingSubject.onNext(true)
        return subject
    }

    private fun <T> completeSubject(subject: PublishSubject<T>) {
        subject.onCompleted()
        subjectsMap.remove(subject)
        if (subjectsMap.isEmpty()) {
            loadingSubject.onNext(false)
        }
    }

    class BasicOnCompleteListener<T>(val result: T, val subject: PublishSubject<T>) : Firebase.CompletionListener {

        override fun onComplete(firebaseError: FirebaseError?, p1: Firebase?) {
            if (firebaseError != null) {
                basicErrorMethod(firebaseError, subject)
            } else {
                basicSuccessMethod(result, subject)
            }
        }

    }
}