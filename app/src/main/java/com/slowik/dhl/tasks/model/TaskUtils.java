package com.slowik.dhl.tasks.model;

import java.util.Arrays;
import java.util.List;

public class TaskUtils {

    public static final String STATUS_ON_THE_WAY = "On the way";
    public static final String STATUS_PARKING = "Parking";
    public static final String STATUS_WAITING_FOR_UNLOADING = "Not started";
    public static final String STATUS_STARTED_UNLOADING = "Unloading";
    public static final String STATUS_ALMOST_FINISHED_UNLOADING = "Almost unloaded";
    public static final String STATUS_FINISHED_UNLOADING = "Unloading Done";
    public static final String STATUS_STARTED_SENDING = "Sending";
    public static final String STATUS_FINISHED_SENDING = "Sending Done";
    public static final String STATUS_STARTED_PUTAWAY = "Putaway";
    public static final String STATUS_FINISHED = "Done";

    public static final String ACTION_CREATE_TASK = "Create task";
    public static final String ACTION_ARRIVED = "Mark as arrived";
    public static final String ACTION_ADD_GATE = "Move on gate";
    public static final String ACTION_START_UNLOADING = "Start unloading";
    public static final String ACTION_ALMOST_FINISH_UNLOADING = "20 min to end";
    public static final String ACTION_FINISH_UNLOADING = "Finish unloading";
    public static final String ACTION_START_SENDING = "Start sending";
    public static final String ACTION_FINISH_NEXT_SENDING = "Finish next line";
    public static final String ACTION_START_PUTAWAY = "Start putaway";
    public static final String ACTION_FINISH = "Finish putaway";
    public static final String ACTION_UNDO = "Undo";

    public static final List<String> statuses = Arrays.asList(
            STATUS_ON_THE_WAY,
            STATUS_PARKING,
            STATUS_WAITING_FOR_UNLOADING,
            STATUS_STARTED_UNLOADING,
            STATUS_ALMOST_FINISHED_UNLOADING,
            STATUS_FINISHED_UNLOADING,
            STATUS_STARTED_SENDING,
            STATUS_FINISHED_SENDING,
            STATUS_STARTED_PUTAWAY,
            STATUS_FINISHED);

    public static final List<String> actions = Arrays.asList(
            ACTION_ARRIVED,
            ACTION_ADD_GATE,
            ACTION_START_UNLOADING,
            ACTION_ALMOST_FINISH_UNLOADING,
            ACTION_FINISH_UNLOADING,
            ACTION_START_SENDING,
            ACTION_FINISH_NEXT_SENDING,
            ACTION_START_PUTAWAY,
            ACTION_FINISH,
            ACTION_UNDO);
}
