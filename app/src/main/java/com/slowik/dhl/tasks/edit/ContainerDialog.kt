package com.slowik.dhl.tasks.edit

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.widget.EditText
import com.slowik.dhl.R

class ContainerDialog(val onClick: (name: String) -> Unit, val oldName: String?) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog? {

        val builder = AlertDialog.Builder(getActivity());
        val inflater = getActivity().getLayoutInflater();

        val view = inflater.inflate(R.layout.dialog_container, null)

        val nameET = view.findViewById(R.id.nameET) as EditText

        nameET.setText(oldName)

        builder.setView(view).setPositiveButton("OK", { dialog, id -> onClick(nameET.text.toString()) })
        return builder.create();
    }

}