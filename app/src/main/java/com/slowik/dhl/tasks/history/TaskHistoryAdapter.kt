package com.slowik.dhl.tasks.history

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.slowik.dhl.R
import com.slowik.dhl.tasks.model.TaskAction
import java.text.SimpleDateFormat
import java.util.*


class TaskHistoryAdapter(val activity: FragmentActivity, private var mDataset: MutableList<TaskAction>) : RecyclerView.Adapter<TaskHistoryAdapter.ViewHolder>() {

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        lateinit var usernameTV: TextView
        lateinit var actionTV: TextView
        lateinit var dateTV: TextView

        init {
            usernameTV = v.findViewById(R.id.userTV) as TextView
            actionTV = v.findViewById(R.id.actionTV) as TextView
            dateTV = v.findViewById(R.id.dateTV) as TextView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_task_history, parent, false)
        val vh = ViewHolder(v)
        return vh
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val taskAction = mDataset[position]

        holder.usernameTV.text = taskAction.submitedBy
        val fmtOut = SimpleDateFormat("HH:mm dd-MM-yyyy");
        holder.dateTV.text = fmtOut.format(Date(taskAction.submitedTimestamp));
        holder.actionTV.text = taskAction.actionType

    }

    override fun getItemCount(): Int {
        return mDataset.size
    }

    fun updateItems(items: MutableList<TaskAction>) {
        mDataset = items
        notifyDataSetChanged()
    }


}
