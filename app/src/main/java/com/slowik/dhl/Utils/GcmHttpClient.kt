package com.slowik.dhl.Utils

import android.util.Log
import com.google.gson.Gson
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import com.slowik.dhl.App
import com.slowik.dhl.tasks.model.Task
import cz.msebera.android.httpclient.entity.StringEntity
import org.json.JSONObject


object GcmHttpClient {

    val client = AsyncHttpClient()

    init {
        client.addHeader("Authorization", "key=AIzaSyD8Y5-M720kqNMI8gtsNLFnhLkszXuAmuI")
    }

    fun newAction(task: Task) {
        client.post(App.context, "https://gcm-http.googleapis.com/gcm/send", createEntity(task), "application/json", object : JsonHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Array<out cz.msebera.android.httpclient.Header>?, response: JSONObject?) {
                Log.d("mytest", response.toString())
            }
        })
    }

    fun createEntity(task: Task): StringEntity {
        val sendingTask = Task(task)
        sendingTask.actions = arrayListOf(task.actions.last())

        val jsonParams = JSONObject()
        jsonParams.put("to", "/topics/newAction");
        val data = JSONObject()
        data.put("message", Gson().toJson(sendingTask));
        jsonParams.put("data", data);
        return StringEntity(jsonParams.toString());
    }

}