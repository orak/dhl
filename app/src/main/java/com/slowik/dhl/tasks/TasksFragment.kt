package com.slowik.dhl.tasks

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.client.ChildEventListener
import com.firebase.client.DataSnapshot
import com.firebase.client.Firebase
import com.firebase.client.FirebaseError
import com.google.gson.Gson
import com.kogitune.activity_transition.ActivityTransitionLauncher
import com.slowik.dhl.App
import com.slowik.dhl.R
import com.slowik.dhl.Utils.FirebaseOperator
import com.slowik.dhl.Utils.Prefs
import com.slowik.dhl.tasks.edit.EditTaskActivity
import com.slowik.dhl.tasks.model.Task
import com.slowik.dhl.tasks.model.TaskUtils
import rx.subjects.PublishSubject
import java.util.*

class TasksFragment : Fragment() {

    private var ref: Firebase? = null
    private var listener: ChildEventListener? = null

    private var adapter: TaskAdapter? = null
    private val items: MutableList<Task> = arrayListOf()
    private val itemsMap = hashMapOf<String?, Task>()

    val clickSubject: PublishSubject<TaskAdapter.TaskListClickInfo> = PublishSubject.create()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_fab_list, container, false)

        return view
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fab = view?.findViewById(R.id.fab)
        val recyclerView = view?.findViewById(R.id.recyclerView) as RecyclerView

        fab?.setOnClickListener {
            startActivity(Intent(activity, AddTaskActivity::class.java))
        }
        if (!App.isAdmin(Prefs.load(activity, "username"))) {
            fab?.visibility = View.GONE
        } else {
            clickSubject.subscribe { clickInfo ->
                val intent = Intent(activity, EditTaskActivity::class.java)
                val json = Gson().toJson(clickInfo.task)
                intent.putExtra("task", json)
                ActivityTransitionLauncher.with(activity).from(clickInfo.view).launch(intent)
            }
        }

        recyclerView.setHasFixedSize(true)
        val llm = LinearLayoutManager(context);
        recyclerView.setLayoutManager(llm);
        adapter = TaskAdapter(activity, items as ArrayList<Task>, clickSubject)
        recyclerView.setAdapter(adapter)

        ref = FirebaseOperator.firebaseRef.child("tasks")
        listener = object : ChildEventListener {
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onCancelled(p0: FirebaseError?) {
            }

            override fun onChildAdded(snapshot: DataSnapshot?, p1: String?) {
                val data = snapshot?.getValue(Task::class.java)
                if (data != null) {
                    data.uid = snapshot?.key
                    var index = items.size
                    index = correctPositionIfNotFinished(data, index)
                    items.add(index, data)
                    itemsMap.put(data.uid, data)
                    adapter?.notifyDataSetChanged()
                }
            }


            override fun onChildChanged(snapshot: DataSnapshot?, p1: String?) {
                val data = snapshot?.getValue(Task::class.java)
                if (data != null) {
                    data.uid = snapshot?.key
                    var index = items.indexOfRaw(itemsMap[data.uid])
                    val finished = itemsMap[data.uid]?.actualStatus == TaskUtils.STATUS_FINISHED
                    if (index > -1) {
                        items.removeAt(index)
                        itemsMap.remove(data.uid)
                        index = correctPositionIfFinished(data, index)
                        if (finished) {
                            index = correctPositionIfNotFinished(data, index)
                        }
                        items.add(index, data)
                        itemsMap.put(data.uid, data)
                        adapter?.notifyDataSetChanged()
                    }
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot?) {
                val data = snapshot?.getValue(Task::class.java)
                if (data != null) {
                    data.uid = snapshot?.key
                    items.removeRaw(itemsMap.get(data.uid))
                    itemsMap.remove(data.uid)
                    adapter?.notifyDataSetChanged()
                }
            }
        }
        ref?.addChildEventListener(listener)
    }

    private fun correctPositionIfNotFinished(data: Task, index: Int): Int {
        var index1 = index
        if (data.actualStatus != TaskUtils.STATUS_FINISHED) {
            val firstFinished = items.firstOrNull { task -> task.actualStatus == TaskUtils.STATUS_FINISHED }
            if (firstFinished != null) {
                val firstFinishedIndex = items.indexOfRaw(firstFinished)
                if (firstFinishedIndex > -1) {
                    index1 = firstFinishedIndex
                }
            }
        }
        return index1
    }

    private fun correctPositionIfFinished(data: Task, index: Int): Int {
        var index1 = index
        if (data.actualStatus == TaskUtils.STATUS_FINISHED) {
            val lastNotFinished = items.lastOrNull { task -> task.actualStatus != TaskUtils.STATUS_FINISHED }
            if (lastNotFinished != null) {
                val lastNotFinishedIndex = items.indexOfRaw(lastNotFinished)
                if (lastNotFinishedIndex > -1) {
                    index1 = lastNotFinishedIndex + 1
                }
            }
        }
        return index1
    }

}