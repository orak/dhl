package com.slowik.dhl.tasks

import android.content.Context
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import com.slowik.dhl.App
import com.slowik.dhl.R
import com.slowik.dhl.Utils.FirebaseOperator
import com.slowik.dhl.Utils.GcmHttpClient
import com.slowik.dhl.Utils.Prefs
import com.slowik.dhl.tasks.TaskView.inflater
import com.slowik.dhl.tasks.model.Task
import com.slowik.dhl.tasks.model.TaskAction
import com.slowik.dhl.tasks.model.TaskUtils
import com.slowik.dhl.users.AddGateDialog
import com.slowik.dhl.users.OccupiedLinesDialog
import java.text.SimpleDateFormat
import java.util.*


class TaskView : FrameLayout {

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
    }

    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0) {
    }

    constructor(context: Context) : this(context, null, 0) {
    }

    lateinit var dateTV: TextView
    lateinit var statusUserTV: TextView
    lateinit var containerNumberTV: TextView
    lateinit var gateTV: TextView
    lateinit var statusTitleTV: TextView
    lateinit var statusDetailsTV: TextView
    lateinit var undoButton: Button
    lateinit var actionButton: Button
    lateinit var cardView: CardView

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.item_task, this, true)

        dateTV = findViewById(R.id.dateTV) as TextView
        containerNumberTV = findViewById(R.id.containerTV) as TextView
        statusUserTV = findViewById(R.id.statusUserTV) as TextView
        gateTV = findViewById(R.id.gateTV) as TextView
        statusTitleTV = findViewById(R.id.statusTitleTV) as TextView
        statusDetailsTV = findViewById(R.id.statusDetailsTV) as TextView
        undoButton = findViewById(R.id.undoButton) as Button
        actionButton = findViewById(R.id.actionButton) as Button
        cardView = findViewById(R.id.cv) as CardView
    }

    fun bindData(task: Task?, activity: FragmentActivity) {
        if (task != null) {

            val dateFormat = SimpleDateFormat("dd.MM.yyyy")
            val date = dateFormat.format(Date(task.createTimestamp))
            dateTV.text = date
            containerNumberTV.setText(task.containerNumber)
            if (!task.gate.isEmpty()) {
                gateTV.visibility = View.VISIBLE
                gateTV.text = task.gate
            } else {
                gateTV.visibility = View.GONE
            }
            if (task.actions.size > 0) {
                statusUserTV.text = "by " + task.actions.filter { action -> action.actionType != TaskUtils.ACTION_UNDO }.last().submitedBy
                statusUserTV.visibility = View.VISIBLE
            } else {
                statusUserTV.visibility = View.GONE
            }
            statusTitleTV.text = task.actualStatus
            if (task.actualStatus == TaskUtils.STATUS_FINISHED_UNLOADING) {
                statusDetailsTV.visibility = View.VISIBLE
                statusDetailsTV.text = "Occupied " + task.linesOccupied + " lines"
            } else if ( task.actualStatus == TaskUtils.STATUS_STARTED_SENDING) {
                statusDetailsTV.visibility = View.VISIBLE
                statusDetailsTV.text = "Done " + task.linesSent + "/" + task.linesOccupied + " lines"
            } else {
                statusDetailsTV.visibility = View.GONE
            }

            val statusIndex = TaskUtils.statuses.indexOf(task.actualStatus)
            actionButton.text = TaskUtils.actions[statusIndex]

            if (task.actualStatus == TaskUtils.STATUS_ON_THE_WAY ||
                    (task.actualStatus == TaskUtils.STATUS_PARKING && !App.isAdmin(Prefs.load(activity, "username"))) ||
                    (task.actualStatus == TaskUtils.STATUS_WAITING_FOR_UNLOADING && !App.isAdmin(Prefs.load(activity, "username")))) {
                undoButton.visibility = View.GONE
            } else {
                undoButton.visibility = View.VISIBLE
            }

            if (task.actualStatus == TaskUtils.STATUS_FINISHED) {
                actionButton.visibility = View.GONE
            } else {
                actionButton.visibility = View.VISIBLE
            }

            if (App.isAdmin(Prefs.load(activity, "username"))) {
                actionButton.visibility = View.VISIBLE
                if (task.actualStatus == TaskUtils.STATUS_FINISHED) {
                    actionButton.text = "Delete"
                }
            }

            actionButton.setOnClickListener {
                if (task.actualStatus == TaskUtils.STATUS_FINISHED) {
                    FirebaseOperator.removeValue("tasks/" + task.uid)
                } else if (task.actualStatus == TaskUtils.STATUS_PARKING) {
                    val dialog = AddGateDialog({ gate ->
                        task.gate = gate
                        task.actions.add(TaskAction(TaskUtils.actions[statusIndex], Prefs.load(activity, "username") ?: "", System.currentTimeMillis()))
                        task.actualStatus = TaskUtils.statuses[statusIndex + 1]
                        setActionInDatabase(task)
                    })
                    dialog.show(activity.supportFragmentManager, "tag2")
                } else if (task.actualStatus == TaskUtils.STATUS_STARTED_SENDING) {
                    task.linesSent++
                    task.actions.add(TaskAction(TaskUtils.ACTION_FINISH_NEXT_SENDING, Prefs.load(activity, "username") ?: "", System.currentTimeMillis()))
                    if (task.linesSent == task.linesOccupied) {
                        task.actualStatus = TaskUtils.STATUS_FINISHED_SENDING
                    }
                    setActionInDatabase(task)
                } else if (task.actualStatus == TaskUtils.STATUS_ALMOST_FINISHED_UNLOADING) {
                    val dialog = OccupiedLinesDialog({ linesOccupied ->
                        if (!linesOccupied.isEmpty()) {
                            task.linesOccupied = linesOccupied.toInt()
                            task.actions.add(TaskAction(TaskUtils.actions[statusIndex], Prefs.load(activity, "username") ?: "", System.currentTimeMillis()))
                            if (task.linesOccupied == 0) {
                                task.actualStatus = TaskUtils.STATUS_FINISHED_SENDING
                            } else {
                                task.actualStatus = TaskUtils.statuses[statusIndex + 1]
                            }
                            setActionInDatabase(task)
                        }
                    })
                    dialog.show(activity.supportFragmentManager, "tag")
                } else {
                    task.actions.add(TaskAction(TaskUtils.actions[statusIndex], Prefs.load(activity, "username") ?: "", System.currentTimeMillis()))
                    task.actualStatus = TaskUtils.statuses[statusIndex + 1]
                    setActionInDatabase(task)
                }
            }

            undoButton.setOnClickListener {
                if (task.actualStatus == TaskUtils.STATUS_STARTED_SENDING) {
                    task.linesSent--
                    task.actions.add(TaskAction(TaskUtils.ACTION_UNDO, Prefs.load(activity, "username") ?: "", System.currentTimeMillis()))
                    if (task.linesSent == -1) {
                        task.linesSent = 0
                        task.actualStatus = TaskUtils.STATUS_FINISHED_UNLOADING
                    }
                    setActionInDatabase(task)

                } else if (task.actualStatus == TaskUtils.STATUS_FINISHED_SENDING && task.linesOccupied == 0) {
                    task.actions.add(TaskAction(TaskUtils.ACTION_UNDO, Prefs.load(activity, "username") ?: "", System.currentTimeMillis()))
                    task.actualStatus = TaskUtils.STATUS_ALMOST_FINISHED_UNLOADING
                    setActionInDatabase(task)

                } else {
                    if (task.actualStatus == TaskUtils.STATUS_FINISHED_SENDING) {
                        task.linesSent--
                    } else if (task.actualStatus == TaskUtils.STATUS_WAITING_FOR_UNLOADING) {
                        task.gate = ""
                    }
                    task.actions.add(TaskAction(TaskUtils.ACTION_UNDO, Prefs.load(activity, "username") ?: "", System.currentTimeMillis()))
                    task.actualStatus = TaskUtils.statuses[statusIndex - 1]
                    setActionInDatabase(task)
                }
            }
        }
    }

    private fun setActionInDatabase(task: Task) {
        FirebaseOperator.setValue("tasks/" + task.uid, task)
        GcmHttpClient.newAction(task)
    }
}