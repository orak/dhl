package com.slowik.dhl.users

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.cocosw.bottomsheet.BottomSheet
import com.slowik.dhl.R
import com.slowik.dhl.Utils.FirebaseOperator


class UserAdapter(val activity: FragmentActivity, private val mDataset: MutableList<UserAdapter.UserData>) : RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    data class UserData(val username: String, val password: String, var uid: String?) {
        constructor() : this("", "", "")
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        lateinit var usernameTV: TextView
        lateinit var passwordTV: TextView
        lateinit var moreIV: ImageView

        init {
            usernameTV = v.findViewById(R.id.usernameTV) as TextView
            passwordTV = v.findViewById(R.id.passTV) as TextView
            moreIV = v.findViewById(R.id.moreButton) as ImageView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        val vh = ViewHolder(v)
        return vh
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val username = mDataset[position].username
        val password = mDataset[position].password
        val uid = mDataset[position].uid
        holder.usernameTV.setText(username)
        holder.passwordTV.setText("pass: " + mDataset[position].password)
        holder.moreIV.setOnClickListener {
            BottomSheet.Builder(activity).title(username).sheet(R.menu.basic_list).listener { dialog, which ->
                if (which == R.id.delete) {
                    FirebaseOperator.removeUser(username, password, uid!!)
                } else if (which == R.id.edit) {
                    showEditUserDialog(username, password, uid!!)
                }
            }.show()
        }
    }

    override fun getItemCount(): Int {
        return mDataset.size
    }

    private fun showEditUserDialog(username: String, password: String, uid: String) {
        val dialog = UserDialog("Edit", { newusername, newpassword ->
            if (!newusername.isEmpty() && !newpassword.isEmpty()) {
                FirebaseOperator.removeUser(username, password, uid!!)
                FirebaseOperator.createUser(newusername, newpassword)
            }
        }, username, password)
        dialog.show(activity.supportFragmentManager, "tag")
    }
}
