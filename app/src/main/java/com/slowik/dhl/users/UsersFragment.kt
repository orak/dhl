package com.slowik.dhl.users

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.client.ChildEventListener
import com.firebase.client.DataSnapshot
import com.firebase.client.Firebase
import com.firebase.client.FirebaseError
import com.slowik.dhl.R
import com.slowik.dhl.Utils.FirebaseOperator
import java.util.*

class UsersFragment : Fragment() {

    private var ref: Firebase? = null
    private var listener: ChildEventListener? = null

    private var adapter: UserAdapter? = null
    private var items: MutableList<UserAdapter.UserData>? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_fab_list, container, false)

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val fab = view?.findViewById(R.id.fab)
        val recyclerView = view?.findViewById(R.id.recyclerView) as RecyclerView

        fab?.setOnClickListener {
            showCreateUserDialog()
        }

        recyclerView.setHasFixedSize(true)
        val llm = LinearLayoutManager(context);
        recyclerView.setLayoutManager(llm);
        items = arrayListOf()
        adapter = UserAdapter(activity, items as ArrayList<UserAdapter.UserData>)
        recyclerView.setAdapter(adapter)

        ref = FirebaseOperator.firebaseRef.child("users")
        listener = object : ChildEventListener {
            override fun onChildMoved(p0: DataSnapshot?, p1: String?) {
            }

            override fun onCancelled(p0: FirebaseError?) {
            }

            override fun onChildAdded(snapshot: DataSnapshot?, p1: String?) {
                val userData = snapshot?.getValue(UserAdapter.UserData::class.java)
                if (userData != null) {
                    userData.uid = snapshot?.key
                    items?.add(userData)
                    adapter?.notifyDataSetChanged()
                }
            }

            override fun onChildChanged(snapshot: DataSnapshot?, p1: String?) {

            }

            override fun onChildRemoved(snapshot: DataSnapshot?) {
                val userData = snapshot?.getValue(UserAdapter.UserData::class.java)
                if (userData != null) {
                    userData.uid = snapshot?.key
                    items?.remove(userData)
                    adapter?.notifyDataSetChanged()
                }
            }
        }
        ref?.addChildEventListener(listener)
    }

    private fun showCreateUserDialog() {
        val dialog = UserDialog("Create", { username, password -> FirebaseOperator.createUser(username, password) })
        dialog.show(activity.supportFragmentManager, "tag")
    }

}