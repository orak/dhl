package com.slowik.dhl.tasks.edit

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.widget.ArrayAdapter
import com.slowik.dhl.R
import com.slowik.dhl.Utils.FirebaseOperator
import com.slowik.dhl.users.DriverAdapter
import fr.ganfra.materialspinner.MaterialSpinner
import java.util.*

class DriversDialog(val onClick: (firstDriver: String, secondDriver: String) -> Unit, val firstDriverUid: String?, val secondDriverUid: String?) : DialogFragment() {

    private val driversMap: HashMap<String, DriverAdapter.DriverData> = hashMapOf()

    private var firstDriver: String? = null
    private var secondDriver: String? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog? {

        val builder = AlertDialog.Builder(getActivity());
        val inflater = getActivity().getLayoutInflater();

        val view = inflater.inflate(R.layout.dialog_drivers, null)

        val driverSpinner1 = view.findViewById(R.id.driverSpinner1) as MaterialSpinner?
        val driverSpinner2 = view.findViewById(R.id.driverSpinner2) as MaterialSpinner?

        val drivers = arrayListOf<String>()
        FirebaseOperator.oneShotRequest("drivers").subscribe { snapshot ->
            for (subsnapshot in snapshot.children) {
                val data = subsnapshot?.getValue(DriverAdapter.DriverData::class.java)
                if (data != null) {
                    data.uid = subsnapshot.key
                    val displayData = data.name + " (" + data.phone + ")"
                    driversMap.put(displayData, data)
                    drivers.add(displayData)
                    if (data.uid == firstDriverUid) {
                        firstDriver = displayData
                    }
                    if (data.uid == secondDriverUid) {
                        secondDriver = displayData
                    }
                }
            }
            populateSpinner(drivers, driverSpinner1)
            populateSpinner(drivers, driverSpinner2)

            val index1 = drivers.indexOfRaw(firstDriver) + 1
            val index2 = drivers.indexOfRaw(secondDriver) + 1
            driverSpinner1?.setSelection(index1)
            driverSpinner2?.setSelection(index2)
        }

        builder.setView(view).setPositiveButton("OK", {
            dialog, id ->
            onClick(
                    driversMap.get(driverSpinner1?.selectedItem as String)?.uid ?: "",
                    driversMap.get(driverSpinner2?.selectedItem as String)?.uid ?: "")
        })
        return builder.create();
    }

    private fun populateSpinner(items: List<String>, spinner: MaterialSpinner?) {
        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, items)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner?.setAdapter(adapter)
    }

}